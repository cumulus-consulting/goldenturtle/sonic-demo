# SONIC In The Cloud

This demo represents the `SONIC in the Cloud` topology on the NVIDIA Air Infrastructure Simulation Platform.

This architecture is based on the cldemo2 topology: https://gitlab.com/cumulus-consulting/goldenturtle/cldemo2

Instead of using Cumulus Linux, it uses SONIC as the NOS for all the nodes in the topology.

This demo is an unconfigured environment of all SONIC nodes.

For a pre-configured demo that uses the same topology but implements a layer 3 fabric, use the demo marketplace simulation named `SONiC L3 Fabric with Host Based Networking`

## Login Details

The login for the Out of Band Server;

| Credential | Value |
|----------|:--------|
| Username | cumulus |
| Password | CumulusLinux! |

The login for the SONIC nodes:

| Credential | Value |
|----------|:--------|
| Username | admin |
| Password | YourPaSsWoRd |

This is the default username and password for SONIC images, as set by the upstream repository.

